//Cathy Tham, 1944919

package movies.importer;
import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	
	public LowercaseProcessor(String srcDir, String outputDir) {
		super(srcDir, outputDir, true); 
		
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i=0; i<input.size() ; i++) {
			String lowerCase=input.get(i).toLowerCase();
			asLower.add(lowerCase);
		}
		
    return asLower;     
	}

	

}

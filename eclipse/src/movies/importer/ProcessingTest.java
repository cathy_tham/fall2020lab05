//Cathy Tham, 1944919

package movies.importer;

import java.io.IOException;


public class ProcessingTest {

	public static void main(String[] args) throws IOException {
		String source = "C:\\cathy\\courses\\java310\\fall2020lab05\\testInput";
		String destination ="C:\\cathy\\courses\\java310\\fall2020lab05\\testOutput";
		
		LowercaseProcessor test = new LowercaseProcessor(source, destination);
		test.execute();
		
		String sourceNoDup = "C:\\cathy\\courses\\java310\\fall2020lab05\\testOutput";
		String destinationNoDup ="C:\\cathy\\courses\\java310\\fall2020lab05\\noDuplicate";
		
		RemoveDuplicates noDuplicateTest = new RemoveDuplicates(sourceNoDup, destinationNoDup);
		noDuplicateTest.execute();
		
	}
		

	
	

}

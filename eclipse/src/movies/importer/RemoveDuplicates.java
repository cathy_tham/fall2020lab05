//Cathy Tham, 1944919

package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	
	public RemoveDuplicates(String srcDir, String outputDir) {
		super(srcDir, outputDir, false); 
		
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> noDuplicate = new ArrayList<String>();
		
		for(int i=0; i< input.size() ; i++) {
			//If old array doesn't exist in the new array
			if(!(noDuplicate.contains(input.get(i)))) {
				noDuplicate.add(input.get(i));
			}
		}
		
		return noDuplicate;
		
	}
}
  
